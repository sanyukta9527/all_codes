#include <stdio.h>

void main(){
	int r;
	printf("Enter the rows :");
	scanf("%d",&r);

	for(int i=0;i<r;i++){
		int a=65;
		for(int j=0;j<r;j++){
			if(j<r-i-1){
				printf("  ");
			}else{
				if(j%2==0){
				printf("%c ",a);
				}else{
					printf("%c ",a+32);
				} 
			}
			a++;
		}
		printf("\n");
	}
}