// sum of number in given range 

#include<stdio.h>
void main(){
	int a;
	printf("Enter the number :");
	scanf("%d",&a);

	int temp=a;
	int rem=0;
	int sum=0;

	while(temp>0){
		rem=temp%10;
		sum=sum+rem;
		temp=temp/10;
	}
	printf("%d is the sum of the digit present in number %d  \n ",sum,a);
}