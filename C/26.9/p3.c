# include<stdio.h>

void main(){
	int r;
	printf("Enter number of rows:");
	scanf("%d",&r);

	for(int i=0;i<r;i++){
		int a=1;
		for(int j=0 ; j<r+i; j++){
			if(j<r-i-1){
				printf("  ");
			}else{
				printf("%d ",a);
				a++;
			}
		}
		printf("\n");
	}
}