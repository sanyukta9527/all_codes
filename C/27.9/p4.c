#include<stdio.h>

void main(){
	int r;
	printf("Enter the rows : ");
	scanf("%d",&r);

	int n=2*r-1;

	for(int i=0;i<r;i++){
		n=2*r-1;
		for(int j=0;j<2*r-1;j++){
			if(j<2*i){
				printf("   ");
			}else{
				printf(" %d ",n);
			}
			n--;
		}
		printf("\n");
	}
}