// WAP to print factorial of even digits in a given number 

#include<stdio.h>
void main(){
	int n;
	printf("Enter the number :");
	scanf("%d",&n);

	int rem;
	while(n>0){
		rem=n%10;
		int fact=1;
		if(rem%2==0){
			if(rem==0){
				printf("Factorial of 0 is 1 \n");
			}else{
				for(int i=1;i<=rem;i++){
					fact=fact*i;
				}
				printf("Factorial of %d is %d \n",rem , fact);
			}
		}
		n=n/10;
	}
} 