/*
0	0	0	0	0
	1	2	3	4
		4	6	8
			9	12
				16
				*/
#include <stdio.h>
void main(){
	int r;
	printf("Enter rows : ");
	scanf("%d",&r);

	for(int i=0;i<r;i++){
		for(int j=0;j<r;j++){
			if(j<i){
				printf("  ");
			}else{
				printf("%d ",i*j);
			}
		}
		printf("\n");
	}


}