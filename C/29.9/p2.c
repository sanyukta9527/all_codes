/*
     1
  3  1
5 3  1
  3  1
     1

*/

#include <stdio.h>
void main(){
	int n;
	int r;
	printf("Enter rows : ");
	scanf("%d",&r);
	int a=0;
	for(int i=0;i<r;i++){
		if(i<r/2){
			n=1+2*i;
		}else{
			n=2*(r-i)-1;
		}
		for(int j=0;j<r/2+1;j++){
			if(i<(r/2)){
				if(j<(r/2)-i){
					printf("   ");
				}else{
					printf(" %d ",n);
					n=n-2;
				}
			}else{
				
				if(j<a){
					printf("   ");	
				}else{
					printf(" %d ",n);
					n=n-2;
				}
			}

		}
		if(i>=(r/2)){
			a=a+1;
		}
		printf("\n");
	}
}
