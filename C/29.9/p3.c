/*
1  2  3  4
4  1  2  3
3  4  1  2
2  3  4  1
*/

#include<stdio.h>
void main(){
	int r;
	printf("Enter rows : ");
	scanf("%d",&r);
	int n=1;

	for(int i=0;i<r;i++){
		for(int j=0;j<r;j++){
			printf(" %d ",n);
			n++;
			if(i-1==j ){
				n=1;
			}
		}
		n--;
		printf("\n");
		
	}

}