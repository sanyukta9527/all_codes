/*
        1
     2	4
  3  6  9
4 8  12 16
  5  10 15
     6  12
        7
*/

#include <stdio.h>
void main(){
	int n=1;
	int r;
	printf("Enter rows : ");
	scanf("%d",&r);
	int a=0;
	for(int i=0;i<r;i++){
		n=1;
		for(int j=0;j<r/2+1;j++){
			if(i<(r/2)){
				if(j<(r/2)-i){
					printf("   ");
				}else{
					printf(" %d ",(i+1)*n);
					n++;
				}
			}else{
				if(j<a){
					printf("   ");	
				}else{
					printf(" %d ",(i+1)*n);
					n++;
				}
			}

		}
		if(i>=(r/2))
			a=a+1;
		printf("\n");
	}
}

