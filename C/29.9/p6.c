// max digit in a number 
#include<stdio.h>
void main(){
	int n;
	printf("Enter the number :");
	scanf("%d",&n);
	int rem=0;
	int temp=n;
	int max=0;

	while(temp>0){
		rem=temp%10;
		if(max<rem){
			max=rem;
		}
		temp=temp/10;
	}
	printf("Maximum digit in %d is %d \n",n,max);
}