// find radius when area is given 

#include<stdio.h>
void main(){
	float a;
	printf("Enter the area of circle : ");
	scanf("%f",&a);
	float num=(a/3.14);
	float sqrt;
	float temp;
	sqrt =num/2;
	temp=0;

	while(temp!=sqrt){
		temp=sqrt;
		sqrt=((num/temp)+temp)/2;
	}
	printf("The radius of the circle with area %f is %f \n",a,sqrt);
}