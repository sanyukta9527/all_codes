// second min digit in a number 

#include<stdio.h>
void main(){
	int n,rem,min,temp,smin;
	scanf("%d",&n);
	temp=n;
	rem=0;
	smin=0;
	min=9;

	while(temp>0){
		rem=temp%10;
		if(rem<min){
			smin=min;
			min=rem;
		}
		temp=temp/10;
	}
	printf(" second minimum digit in %d is %d \n",n,smin);
} 