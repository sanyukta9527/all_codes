# include <stdio.h>
void main(){
	int a;
	printf("Enter the size of array ");
	scanf("%d",&a);

	int arr[a];
	printf("Enter the elements of array :");
	for (int i=0;i<a;i++){
		scanf("%d",&arr[i]);
	}

	int c=0;
	int ind ;
	for(int i=0;i<a;i++){
		for(int j=0;j<a;j++){
			if(i==j){
				continue;
			}
			if(c==1){
				break;
			}
			if(arr[i]==arr[j]){
				ind=i;
				c++;
			}
		}

	}
	
	if(c>0){
		printf("first duplicate element is at %d index in array \n",ind);
	}else{
		printf("No duplicate element found ");
	}
}