# include <stdio.h>

void main(){
	int a;
	printf("Enter the size of array : ");
	scanf("%d",&a);
	int arr1[a];
	printf("Enter the elements of array : ");
	for (int i=0 ;i<a;i++){
		scanf("%d",&arr1[i]);
	}

	int target;
	printf("Enter the target : ");
	scanf("%d",&target);
	
	int c=0;
	for( int i=0;i<a;i++){
		for(int j=i;j<a;j++){
			if(i==j){
				continue;
			}
			if(arr1[i]+arr1[j]==target){
				printf("%d and %d are the index \n",i ,j);
				c++;
			}
		}
	}
	if(c==0){
		printf("No target found \n");
	}
}
