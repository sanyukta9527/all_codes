# include <stdio.h>

void main(){
	int a;
	printf("Enter the size of array : ");
	scanf("%d",&a);
	int arr[a];
	printf("Enter the elements of array : ");
	for (int i=0 ;i<a;i++){
		scanf("%d",&arr[i]);
	}
	int b;
	printf("Enter the element to find : ");
	scanf("%d",&b);

	int flag=0;
	int index;
	for(int i=0 ;i<a;i++){
		if(arr[i]==b){
			flag++;
			index= i;
			break;
		}
	}
	if(flag==1){
		printf(" the number %d is found at %d index ", b,index);

	}else{
		printf("the number is not present in array ");
	}
}