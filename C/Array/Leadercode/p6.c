# include <stdio.h>

void main(){
	int a;
	printf("Enter the size of array : ");
	scanf("%d",&a);
	int arr[a];
	printf("Enter the elements of array : ");
	for (int i=0 ;i<a;i++){
		scanf("%d",&arr[i]);
	}
	int b;
	printf("Enter the element to find : ");
	scanf("%d",&b);

	int c=0;
	for (int i=0;i<a;i++){
		if(arr[i]==b){
			c++;
		}
	}
	if(c!=0){
		printf("%d occures %d times in array ",b , c);
	}else{
		printf("%d is not present in array ",b);
	}
}