#include<stdio.h>
void main(){
	int row;
	printf("Enter the number of rows ");
	scanf("%d",&row);

	for(int i =1 ; i<= row ;i++){
		for(int j=row;j>=i;j--){
			printf(" ");
		}
		for(int k=1;k<=(2*i-1);k++){
			printf("*");
		}
		printf("\n");
	}

	for(int i =row-1 ; i>=1 ;i--){
		for(int j=row;j>=i;j--){
			printf(" ");
		}
		for(int k=1;k<=(2*i-1);k++){
			printf("*");
		}
		printf("\n");
	}
}
