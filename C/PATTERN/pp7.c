#include <stdio.h> 
void main(){
	int row ;
	printf("Enter the number of rows : \n " );
	scanf("%d",& row);
	int multi = 1;
	int cnt = 1;
	int n=3; 


	for(int i = 1 ; i<=row ; i++){
		for( int j = row ; j >= i ; j-- ){
			printf("  ");
		}

		for(int k = 1; k<= (2*i-1) ; k++){
			if(cnt%2!=0){
				printf("   3 ");
			}else{
				printf("   %d ",n*multi);
				multi++;
			}
			cnt++;
		}
		printf("\n");
	}
}
