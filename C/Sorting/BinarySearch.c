# include <stdio.h>

void main(){
	int arr[]={5,9,17,23,25,45,59,63,71,89};
	int l=0;
	int r=10;
	int mid;
	int data=55;
	int flag=0;
	while(l<r){
		mid=(l+r)/2;

		if(arr[mid]==data){
			printf("Data found at index %d",mid);
			flag++;
			break;
		}else if(arr[mid]>data){
			r=mid-1;
		}else if(arr[mid]<data){
			l=mid+1;
		}
	}
	if(flag==0){
		printf("Element not found ");
	}

}
