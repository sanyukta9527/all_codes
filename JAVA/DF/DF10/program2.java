import java.io.*;
class p2{
	public static void main (String [] args)throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the first number : ");
		int n1 = Integer.parseInt(br.readLine());
		System.out.println("Enter the Second number : ");
		int n2 = Integer.parseInt(br.readLine());

		System.out.println("Before swap " +  n1 +  "  &  " + n2 );
 		       	
		int temp = n1;
	        n1 = n2 ;
		n2 = temp ;
		          System.out.println ( "After Swap "  + n1 + " & " + n2  );
		       
	}
}	
