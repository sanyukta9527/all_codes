/*Program 1: Write a Program to Swap two entered numbers without using
a third temporary variable.
Input: 10 30
Output:
Before Swap: a = 10 & b = 20
After Swap: a = 20 & b = 10
*/
 
import java.io.*;
class p1{
	public static void main(String [] args )throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number ");
		int a = Integer.parseInt(br.readLine ());

		System.out.println("Enter the number ");
		int b= Integer.parseInt(br.readLine ());

		System.out.println("number before swap " + a  +" & " + b);
		
		a = a+b ;
		b = a-b ;
		a = a-b ;

		System.out.println("number After swap " + a  +" & " + b);

	}
}


