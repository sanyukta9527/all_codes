/*Program 2: Write a Program that calculates Kinetic Energy of an object
with given Mass & Velocity.
{Note: K.E. = ½ * m * v * v }
Input: Mass = 53kg Velocity = 5m/s
Output: Kinetic Energy of that Object is: 662.5
 */



import java.io.*;

class p2 {
	public static void main(String [] args )throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the mass " );
		int  m = Integer.parseInt(br.readLine());

		System.out.println("Enter the Velocity " );
		int  v = Integer.parseInt(br.readLine());


		double KE = 0.5 * m * v*v;

		System.out.println( "Kinetic Energy of that object is : " + KE);
	}
}	
