/*

 Write a Program to Print following Pattern.
7
7 6
6 5 4
4 3 2 1
*/

import java.io.*;

class p4{
	public static void main (String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		int rows = Integer.parseInt(br.readLine());
		int a = 7 ;
         	int n =0;
		for(int i = 1 ; i<=rows; i++){
            		n=0;
			for(int j = 1 ; j<=i ;j++){
				
				System.out.print( a);
				a=a-n;
				n++;
			}

			System.out.println();
		}
	}
}
