/*

Write a Program to Print following Pattern.
A A A A A
B B B B
C C C
D D
E

*/

import java.io.*;

class p3{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of Rows");
		int rows = Integer.parseInt(br.readLine());
		
		char ch = 'A';

		for(int i = 1 ; i<=rows ; i++){
			for(int j = rows ; j>=i ; j--){
		        System.out.print(ch + " ");
		    }
		    System.out.println( );
		    ch++;
		}
	}
}