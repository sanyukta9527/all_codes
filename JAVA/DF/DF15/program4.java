/*

 Write a Program to Print following Pattern.
7
6 5
5 4 3
4 3 2 1

*/

import java.io.*;

class p4{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of Rows");
		int row = Integer.parseInt(br.readLine());

		int a = 7;

		for(int i = 1 ;i<=row ; i++){
			int n = a ;
			for(int j =1 ;j<=i ; j++){
				System.out.print( a--  + " ");

			}
			System.out.println();
		}
	}
}	