/*

Program 5: Write a Program that prints a series of perfect numbers up
to the entered limiting number.
Input: 100
Output: The Series of all perfect number from 1 to 10 is
1, 6, 28

*/

import java.io.*;

class p5{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the start number ");
		int num1 = Integer.parseInt(br.readLine());
        System.out.println("Enter the end number ");
		int num2 = Integer.parseInt(br.readLine());
		System.out.println("The  Series of all perfect number from "+ num1 + "to " + num2 + " is ");
		for (int j = num1 ; j<=num2; j++){	
			int i = 1;
			int sum = 0;
                 while(i<=j/2){
                 	if(j%i == 0){
                 		sum+= i;
                 	}
                 	i++;
                 }
                 	if(sum ==j ){
                 		System.out.print(j+" ");
                 	}
                 }
		}
	}
