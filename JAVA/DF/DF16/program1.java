/*

Program 1: Write a Program that prints a series of odd numbers in reverse order
from the limiting number entered by the user.
Input: 100
Output: 99 97 95 93 . . .. 1

*/

import java.io.*;

class p1{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the start number ");
		int num1 = Integer.parseInt(br.readLine());
        System.out.println("Enter the end number ");
		int num2 = Integer.parseInt(br.readLine());

		for (int i= num1 ; i>=num2; i--){
			if(i%2!=0){
				System.out.print( i + "," );
			}
		}	
	}
}
