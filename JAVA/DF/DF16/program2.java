/*

: Write a Program that prints a series of Even numbers in reverse order
from the limiting number entered by the user.
Input: 100
Output: 100 98 96 . . .. 0: Write a Program that prints a series of Even numbers in reverse order


*/

import java.io.*;

class p2{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the start number ");
		int n1 = Integer.parseInt(br.readLine());
        System.out.println("Enter the end number ");
		int n2 = Integer.parseInt(br.readLine());

		for (int i=n1 ; i>=n2; i--){
			if(i%2==0){
				System.out.print( i + "," );
			}
		}	
	}
}