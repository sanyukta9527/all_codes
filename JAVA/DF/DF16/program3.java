/*

: Write a Program to Convert the Hours entered by the user into
Seconds.
Input: 1hr
Output: 3600 seconds
*/

import java.io.*;

class p3{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of hours : ");
		int H= Integer.parseInt(br.readLine());

		int S = H*60*60;

	    System.out.println(S + "  Seconds ");

	}
}
