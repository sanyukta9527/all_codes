/*

: Write a Program to Print following Pattern.
+ = + =
+ = +
+ =
+

*/


import java.io.*;

class p4{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of rows : ");
		int rows= Integer.parseInt(br.readLine());

		for(int i = 1 ;i<=rows;i++){
			for(int j = rows ; j>=i ; j--){
				if(j%2 != 0){
					System.out.print( " = ");
				}else{ 
				    System.out.print(" + ");
				}

			}
			System.out.println( );
		}
	}
}
