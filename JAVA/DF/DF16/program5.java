/*

Write a Program that calculates the Greatest Common Divisor of two
entered numbers. Input: 25 15 Output: The GCD of 25 & 15 is 5

*/

import java.io.*;

class p5{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the start number ");
		int num1 = Integer.parseInt(br.readLine());
        System.out.println("Enter the end number ");
		int num2 = Integer.parseInt(br.readLine());
        int min;
        int GCD ;

		if(num1>num2){
			 min = num2 ;
		}else{
			min = num1;
		}

        int gcd= 0;
		for( int i = 1 ; i<= min ; i++){
			if(num1%i == 0 && num2%i == 0){
				 gcd= i;
			}
		}
		System.out.println(gcd+ " is GCD of "+ num1 + " and " + num2);
	}
}
