import java.io.*;

class p2{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number to check : ");
		int num= Integer.parseInt(br.readLine());
         char hexa[] = new char[100];
		int temp=num;
		int rem = 0;
		int j=0;
		while(temp>0){
			rem =temp%16;
			if(rem<10){
				hexa[j++]=(char)(48+rem);
			}else{
				hexa[j++]=(char)(55+rem);
			}
			temp/=16;
         }
         System.out.print("Hexadecimal value of ,"+ num + " is ");
         for(int i =j-1;i>=0;i--){
         	System.out.print(hexa[i]);
         }
		}
	}