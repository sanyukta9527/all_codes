import java.io.*;

class p4{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of rows : ");
		int rows = Integer.parseInt(br.readLine());

		for (int i = 0;i<rows;i++){
			char ch ='A';
			ch+=i;
			for(int j =0;j<rows-i;j++ ){
				System.out.print(ch+ " ");
				ch+=2;
			}
			System.out.println();
		} 
	}
}