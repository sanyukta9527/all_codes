import java.io.*;

class p4{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of rows : ");
		int row = Integer.parseInt(br.readLine());
        int a =1;
		for (int i=0;i<row;i++){
			for(int j =0;j<row-i;j++ ){
				System.out.print(7*a + " ");
				a++;
			}
			System.out.println();
		} 
	}
}