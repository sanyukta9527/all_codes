import java.io.*;

class p5{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number : ");
		int n1= Integer.parseInt(br.readLine());
		int add =0;
		for(int i=1;i<n1;i++){
			if(n1%i==0){
				add=add+i;
			}
		}
		System.out.println("The sum of all possible divisior of "+ n1 +" is "+add);
	}
}