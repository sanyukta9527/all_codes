import java.io.*;

class p5{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of rows : ");
		int rows = Integer.parseInt(br.readLine());
        
		for (int i = 0;i<rows;i++){
			for(int k =0 ; k<rows-i-1; k++){
				System.out.print("  ");
			}
			int n = rows-i;
			for(int j =0;j<=i;j++ ){
				System.out.print(n+ " ");
				n++;
			}

			System.out.println();
		} 
	}
}