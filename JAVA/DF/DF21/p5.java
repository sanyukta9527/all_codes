import java.io.*;

class p5{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of rows : ");
		int row = Integer.parseInt(br.readLine());
		System.out.println("Enter the number of col : ");
		int col = Integer.parseInt(br.readLine());

        
		for (int i=0 ; i<row ; i++){
			int n =1;
			for(int j=0 ; j<col ; j++){
				if(i==j){
					System.out.print("= "+ " ");

				}else{
					System.out.print(n + " ");

				}
				n++;
			}
			System.out.println();
		}
	}
}
