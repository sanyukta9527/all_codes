import java.io.*;

class p4{
	public static void main(String[] args )throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		System.out.println("Enter the number of rows : ");
		int rows = Integer.parseInt(br.readLine());
        int a=0;
        int b=1;
        int temp;
		for (int i=0;i<rows;i++){
			for(int k=0 ; k<rows-i-1; k++){
				System.out.print("  ");
			}
		
			for(int j =0;j<=i;j++ ){
				if(j==0){
					System.out.print("="+ " ");
				}else{
				    System.out.print(a+ " ");
				    temp=a;
				    a=a+b;
				    b=temp;	
				}
				}

			System.out.println();
		
	}
}
}