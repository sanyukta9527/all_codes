import java.io.*;
class p4{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader( new InputStreamReader( System.in));
		System.out.println("Enter the Number of rows");
		int Rows = Integer.parseInt(br.readLine() );
		int a = 1;

		for(int i = 1 ; i<= Rows; i++){
			for(int j=1 ; j<= Rows ; j++){
				System.out.print( a +"  ");
				a++;
			}
			System.out.println( );
		}
	}
}
