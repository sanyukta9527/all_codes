import java.util.*;
class p5{
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        System.out.println("Enter marks of 5 subjects (Physics,Chemistry,Biology,Mathematics,Computer) : ");
        int sub1 = obj.nextInt();
        int sub2 = obj.nextInt();
        int sub3 = obj.nextInt();
        int sub4 = obj.nextInt();
        int sub5 = obj.nextInt();
        float sum,per;
        if (sub1 >= 35 && sub2 >= 35 && sub3 >= 35 && sub4 >= 35 && sub5 >= 35) {
             sum = sub1 + sub2 + sub3 + sub4 + sub5;
             per = (sum/500)*100;
            System.out.println("Percentages : "+per);
            if(per>=90 && per<=100){
                System.out.println("Grade A");
            }
            else if(per>=80 && per<=90){
                System.out.println("Grade B");
            }
            else if(per>=70 && per<=80){
                System.out.println("Grade C");
            }
            else if(per>=60 && per<=70){
                System.out.println("Grade D");
            }
            else if(per>=50 && per<=60){
                System.out.println("Grade E");
            }
            else {
                System.out.println("Grade F");
            }
        }
        else{
            System.out.println("Student is fail.");
        }

    }
}

