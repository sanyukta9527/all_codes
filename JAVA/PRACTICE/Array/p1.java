/*
Program 1:
Write a program to find even numbers from the array
Input : [1,2,3,4,6,8,10]
Output : 2 4 6 8 10
*/

import java.util.*;
class p1{
	public static void main(String[] args ){
		
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter the length of array :");
		int a = obj.nextInt();
		int arr[ ]=new int[a];
		int flag=0;
		for( int i =0;i<a;i++){
			System.out.println("Enter the element of array :");
			arr[i]=obj.nextInt();
			if(arr[i]%2==0){
				flag++;
			}

		}
		if(flag>0){
			System.out.println("The even numbers in array are : ");
		}
		else{
			System.out.println("There are no even numbers in array ");

		}

		for(int i =0 ;i<a;i++){
			if(arr[i]%2==0){
			System.out.print ( arr[i] + " ");
		}
	}
		System.out.println();
	}
}