/*
Program 10
Write a program to swap two consecutive numbers in the array
Input : [1,2,3,4,5,6,7,8]
Output:[2,1,4,3,6,5,8,7
*/

import java.util.*;
class p10{
	public static void main(String[] args){
		Scanner obj=new Scanner (System.in);
		System.out.print("Enter the size of array : ");
		
		int a=obj.nextInt();
		int arr[]=new int[a];

		for(int i=0 ;i<a;i++){
			System.out.print("Enter the elements : ");
			arr[i]= obj.nextInt();
			}
		
		int temp;
	
		for(int i=0 ;i<a-1;i++){
			temp=arr[i];
			arr[i]=arr[i+1];
			arr[i+1]=temp;
			i++;
		}
		for(int i =0;i<a;i++){
			System.out.print(arr[i]+ " ");
}
System.out.println();
}
}