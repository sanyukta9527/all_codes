/*
Program 1:
WAP to take character array as input, but only print characters do not print
special characters
Input: a b $ % c & d 1 e
Output : a b c d e

*/

import java.util.*;
class p11{
	public static void main(String[] args){
		Scanner obj=new Scanner (System.in);
		System.out.print("Enter the size of array : ");
		
		int Size=obj.nextInt();
		char arr1[]=new char[Size];

		for(int i=0 ;i<Size;i++){
			System.out.print("Enter the elements : ");
			arr1[i]= obj.next().charAt(0);
			}

		for(int i =0;i<Size ; i++){
			if(((arr1[i]>='A')& (arr1[i]<='Z'))|| ((arr1[i]>='a') &(arr1[i]<='z'))){
				System.out.print(arr1[i]+ " ");

			}
		}
		System.out.println();
	}
}