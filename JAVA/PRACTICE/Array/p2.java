/*
Program 2:
Write a program to find odd numbers from the array
Input : [1,2,3,5,7,9,12]
Output : 1 3 5 7 9
*/

import java.util.*;

class p2{
	public static void main(String[] args){
		Scanner obj=new Scanner(System.in);
		System.out.print("Enter the size of array : ");
		int a = obj.nextInt();
		int arr[]= new int[a];
   		 flag=0;
		for ( int i=0 ; i<a ;i++){
			System.out.print("Enter the element of array : ");
			arr[i]=obj.nextInt();
			if(arr[i]%2!=0){
				flag++;
			}
		if(flag>0){
			System.out.print("Enter the odd element of array : ");
			}
		for( int j=0;j<a;j++ ){
			if(arr[j]%2!=0){
				System.out.print(arr[j] + " ");
			}

		}
		}
		}
}