/*
Program 3:
Write a program to find prime number from the given array
Input : [1,3,2,7,9,10,12]
Output : 2 3 7
*/

import java.util.*;
class p3{
	public static void main(String[] args){
		Scanner obj=new Scanner (System.in);
			System.out.print("Enter the size of array : ");

		int a=obj.nextInt();
		int arr[]=new int[a];

		for(int i=0 ;i<a;i++){
			System.out.print("Enter the elements : ");
			arr[i]= obj.nextInt();

		}
		for(int i=0 ;i<a;i++){
			if(arr[i]<2){
					continue;
			}
			int count=0;
			for(int j=2;j<arr[i];j++){
				if(arr[i]%j==0){
					count++;
				}
			}
			if(count==0){
				System.out.print(arr[i] + " ");
				}
			}
		}
	
}