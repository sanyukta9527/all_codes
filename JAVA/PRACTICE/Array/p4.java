/*
4. Program 4:
Write a program to sum alternate number from the given array
Input : [1,2,3,4,5,6,7]
Output : 16 /// 1 + 3 + 5 + 7
*/

import java.util.*;
class p4{
	public static void main(String[] args){
		Scanner obj=new Scanner (System.in);
		System.out.print("Enter the size of array : ");

		int a=obj.nextInt();
		int arr[]=new int[a];

		for(int i=0 ;i<a;i++){
			System.out.print("Enter the elements : ");
			arr[i]= obj.nextInt();
			}
		int sum=0;
		for(int i=0 ;i<a;i++){
			if(i%2==0){
				sum+=arr[i];
			}
		}
		System.out.println("The sum of alternate elements in array is : " + sum);
	}
}
