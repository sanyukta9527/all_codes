/*
5. Program 5:
Write a program to find the occurrence of a given number from
the array
Input : [1,2,3,4,4,5,6,7]
Occurrence count of number = 4
Output: 2
*/

import java.util.*;
class p5{
	public static void main(String[] args){
		Scanner obj=new Scanner (System.in);
		System.out.print("Enter the size of array : ");

		int a=obj.nextInt();
		int arr[]=new int[a];

		for(int i=0 ;i<a;i++){
			System.out.print("Enter the elements : ");
			arr[i]= obj.nextInt();
			}
		System.out.print("Enter the number to find  : ");
		int find= obj.nextInt();
		int count=0;
		for (int i =0 ; i<a;i++){
			if(find==arr[i]){
				count++;
			}
		}
		if(count>0){
			System.out.println(find + " occurrencre count is  "+ count);
		}
		else{
				System.out.println("number not found ");
			}
		}
		
	}

