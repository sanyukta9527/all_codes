/*
Program 6:
Write a program to find min and max from the given array
Input : [1,43,65,71,89,55]
Output : min = 1
max = 89

*/
import java.util.*;
class p6{
	public static void main(String[] args){
		Scanner obj=new Scanner (System.in);
		System.out.print("Enter the size of array : ");

		int a=obj.nextInt();
		int arr[]=new int[a];

		for(int i=0 ;i<a;i++){
			System.out.print("Enter the elements : ");
			arr[i]= obj.nextInt();
			}
		int min=arr[0];
		int max=arr[0];

		for(int i=0; i<a ;i++){
			if(arr[i]<min){
				min=arr[i];
			}
			if(arr[i]>max){
				max=arr[i];
			}
		}
		System.out.println(min + " is smallest number in array ");
		System.out.println(max + " is largest number in array ");

	}
}