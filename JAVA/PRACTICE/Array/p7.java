/*
Program 7:
Write a program to search a given element and prints its index
Input : [1,2,43,5,66,87,9]
Search = 43
Output : index = 2
*/

import java.util.*;
class p7{
	public static void main(String[] args){
		Scanner obj=new Scanner (System.in);
		System.out.print("Enter the size of array : ");
		int a=obj.nextInt();
		int arr[]=new int[a];

		for(int i=0 ;i<a;i++){
			System.out.print("Enter the elements : ");
			arr[i]= obj.nextInt();
		}

		System.out.print("Enter the number to find  : ");
		int find= obj.nextInt();

		for (int i =0 ; i<a ; i++){
			if(arr[i]==find){
				System.out.println("index : "+ i);
			    break;
		}
	}
	}
}