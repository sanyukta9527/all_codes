/*
. Program 8:
Write a program to reverse a array
Input : [1,2,3,4,5]
Output : [5,4,3,2,1]
*/


import java.util.*;
class p8{
	public static void main(String[] args){
		Scanner obj=new Scanner (System.in);
		System.out.print("Enter the size of array : ");
		int a=obj.nextInt();
		int arr[]=new int[a];

		for(int i=0 ;i<a;i++){
			System.out.print("Enter the elements : ");
			arr[i]= obj.nextInt();
			}
		
		int temp;
		int n=1;
		for(int i=0 ;i<a/2;i++){
			temp=arr[i];
			arr[i]=arr[a-n];
			arr[a-n]=temp;
			n++;
		}
		for(int i =0;i<a;i++){
			System.out.print(arr[i]+ " ");
}
System.out.println();
}
}