/*
Program 2: Write a Program to check whether the number is Negative,
Positive or equal to Zero.
Input: -2
Output: -2 is the negative number
*/

import java.util.*;

class p2{
	public static void main(String [] args ){
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter the intiger : ");
		int num = obj.nextInt();

		if(num>0){
			System.out.println(num + "is positive" );
		} 
		else if (num<0) {
			System.out.println(num + "is negative ");
		}
		else{
			System.out.println(num + " is zero ");
			}
			
		
	}
}