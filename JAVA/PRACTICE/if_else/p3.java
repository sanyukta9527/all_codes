/*
Program 3: Write a Program to Find whether the number Is Even or Odd
Input: 4
Output: 4 is an Even Number!
*/

import java.util.*;

class p3{
	public static void main(String[] arg){
		Scanner obj =new Scanner (System.in);
		System.out.println("Enter the number : ");
		int num= obj.nextInt();

		if(num%2==0){
			System.out.println(num + " is even");
		}
		else{
			System.out.println(num +" is odd");
		}
	}
} 