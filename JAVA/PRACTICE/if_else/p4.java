/*
Program 4: Write a Program to check whether the number is divisible by 5
or not.
Input: 55
Output: 55 is divisible by 5
*/

import java.util.*;
class p4{
	public static void main(String [] args){
		Scanner obj = new Scanner (System.in);
		System.out.println("Enter the number :");
        int num= obj.nextInt();

		if(num%5==0){
			System.out.println(num + " is divisible by 5");
		}
		else{
			System.out.println(num + " is not divisible by 5");

		}
	}
}