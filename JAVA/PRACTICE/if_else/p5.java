/*
Program 5: Write a Program to take an integer ranging from 0 to 6 and print
corresponding weekday (week starting from Monday)
Input: 2
Output: Wednesday.
*/
import java.util.*;
class p5{
	public static void main(String [] args ){
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter the number : ");
		int num = obj.nextInt();

		if(num==0){
			System.out.println( "Monday" );
		} 
		else if (num==1) {
			System.out.println("Tueday");
		}
		else if (num==2) {
			System.out.println("wednesday");
		}
		else if (num==3) {
			System.out.println("Thusday");
		}
		else if (num==4) {
			System.out.println("Friday");
		}
		else if (num==5) {
			System.out.println("Saturday");
		}
		else if (num==6) {
			System.out.println("Sunday");
		}
		else{
			System.out.println( " Invalid input ");
			}
			
		
	}
}