/*
Program 6: Write a Program to check whether the Character is Alphabet or
not.
Input: v
Output: v is an alphabet.
*/

import java.util.*;
class p6{
	public static void main(String [] args ){
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter the char    : ");
		char ch = obj.next().charAt(0);

		if((ch>='A' & ch<='Z') || (ch>='a' & ch<='z')){
			System.out.println(ch + " is alphabet ");
		}
		else{
			System.out.println(ch +" is Not alphabet ");
		}
	}
}

