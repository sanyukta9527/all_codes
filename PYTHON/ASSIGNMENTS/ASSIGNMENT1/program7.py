'''
Enter  the number for 1 to 12 and print months in a year
'''
a = int(input("Enter the number between 1 to 12 :"))


if( a == 1):
	print(" January has 31 days ")
elif( a == 2):
	print(" February has 28/29 days ")
elif( a == 3):
	print(" March has 31 days")
elif( a == 4):
	print(" April has 30 days")
elif( a == 5):
	print(" May has 31 days")
elif( a == 6):
	print(" June has 30 days")
elif( a == 7):
	print(" July has 31 days")
elif( a == 8):
	print(" August has 31 days")
elif( a == 9):
	print(" September has 30 days")
elif( a == 10):
	print(" October has 31 days")
elif( a == 11):
	print(" November has 30 days ")
elif( a == 12):
	print("December has 31 days")
else :
	print ("invalid input")	