'''
WAP to convert all the zeros to ones and news to zeros in given array
ip: [ [1,0,1]
[0,1,1]
[1,0,0] ]
Op: [ [0,1,0]
[1,0,0]
[0,1,1] 
'''

import numpy

r1=int (input("Enter the number of rows : "))
c1=int(input("Enter the number of col : "))

arr = numpy.zeros((r1,c1),int)

for i in range(r1):
	for j in range(c1):
		a = int(input("Enter the element : "))
		arr[i][j] = a 
print(arr)

for i in range(r1):
	for j in range(c1):
		if(arr[i][j]==0):
			arr[i][j]=1
		else:
			arr[i][j]=0

print(arr)

