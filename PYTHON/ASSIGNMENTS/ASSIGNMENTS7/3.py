'''
WAP to take input from the user into an array and remove duplicate
numbers.
Input: [ 1 2 2 3 3 3 4 4 5]
Output: 1 2 3 4 5]
'''
import numpy
r=int (input("Enter the number of rows : "))

arr1= numpy.zeros((r),int)
arr2= numpy.zeros((r),int)

for i in range(r):
	a = int(input("Enter the element : "))
	arr1[i]=a 
print(arr1)
a=0
for i in range(r):
	for j in range(r):
		if(i==j):
			continue
		elif(arr1[i]==arr1[j]):
			arr2[a]=arr1[i]
			a+=1
print(arr2)