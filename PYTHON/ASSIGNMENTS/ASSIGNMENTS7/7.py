'''
Write a java program to take a 2-D array of order 3 X 3 and Sort
that array in ascending order and print it as before and after operation
'''
import numpy

r1=int (input("Enter the number of rows : "))
c1=int(input("Enter the number of col : "))



arr = numpy.zeros((r1,c1),int)


for i in range(r1):
	for j in range(c1):
		ele = int(input("Enter the element : "))
		arr[i][j] = ele

print(arr)

for i in range(len(arr)):
	for j in range (len(arr[0])):
		for a in range(len(arr)):
			for b in range (len(arr[0])):
				if(arr[i][j]<arr[a][b]):
					temp = arr[a][b]
					arr[a][b] = arr[i][j]
					arr[i][j] = temp
print(arr)
