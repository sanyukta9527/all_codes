'''
10. Write a program to convert the string from upper case to
lower case (Implement strlwr()).
Input: DevIce DriVer
Output: device driver
'''

str1=input("Enter the String : ")

str2=""
for i in range (len(str1)):
	if((str1[i]>='A') and (str1[i]<='Z')):
		str2=str2+chr(ord(str1[i])+32)
	else:
		str2=str2+str1[i]
print("Entered string : ",str1)
print("Lower case string :",str2)
