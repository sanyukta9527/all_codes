'''
Write a program to convert the string from lowercase to
uppercase (Implement strupr()).
Input: DevIce DriVer
Output: DEVICE DRIVE
'''
str1=input("Enter the String : ")

str2=""
for i in range (len(str1)):
	if((str1[i]>='a') and (str1[i]<='z')):
		str2=str2+chr(ord(str1[i])-32)
	else:
		str2=str2+str1[i]
print("Entered string : ",str1)
print("Upper case string :",str2)
