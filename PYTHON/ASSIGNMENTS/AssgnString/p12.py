'''
Write a program which toggles the case of a string.
Input: DevIce DriVer
Output: dEViCE dRIvER
'''
str1=input("Enter the String : ")

str2=""
for i in range (len(str1)):
	if((str1[i]>='a') and (str1[i]<='z')):
		str2=str2+chr(ord(str1[i])-32)
	elif((str1[i]>='A') and (str1[i]<='Z')):
		str2=str2+chr(ord(str1[i])+32)
	else:
		str2=str2+str1[i]
print("Entered string : ",str1)
print("Changed string :",str2)
