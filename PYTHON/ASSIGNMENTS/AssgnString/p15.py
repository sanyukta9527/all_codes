'''
. Write a program which accepts strings from the
user and copy first N characters into some destination string
(Implement strncpy()).
Input: India is my Country
N : 8
Output: India is
'''

str1= input("Enter the string : ")
str2=""
n=int(input("Enter the limit  : " ))

if((n>=0) and (n<=len(str1))):
	for i in range (n):
		str2=str2+str1[i]
	print("Entered String :" ,str1 )
	print( "copied string of N numbers :" ,str2)

else:
	print("IndexError")
