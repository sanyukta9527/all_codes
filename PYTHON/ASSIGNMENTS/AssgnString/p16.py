'''
. Write a program which accepts strings from the user and
accept number N then copy the last N character into some other
string.
Input: India is my
N : 5
Output: is my
'''
str1= input("Enter the string : ")
str2=""
n=int(input("Enter the limit  : " ))

if((n>=0) and (n<=len(str1))):
	for i in range (len(str1)-n,len(str1)):
		str2=str2+str1[i]
	print("Entered String " ,str1 )
	print( "copied string of N numbers :" ,str2)

else:
	print("IndexError")
