'''
Write a program which accepts two strings from the user
and compares only first N characters of two strings. If both strings are
equal till first N characters then return 0 otherwise return difference
between first mismatch character (Implement strncmp()).
Input: FirStr FirNew
N : 3
Output: Both strings are equal.
'''
str1= input("Enter the String : ")
str2 = input("Enter the String : ")
n=int(input("Limit of string to compare : "))
count=0
if((n>=0 and n<=len(str1)) and (n>=0 and n<=len(str2))):
	for i in range(n):
		if (str1[i]==str2[i]):
			count+=1
		else:
			if(str1[i]>str2[i]):
				print(ord(str1[i])-ord(str2[i]))
			else:
				print(ord(str2[i])-ord(str1[i]))
			break

	if (count==n):
		print("Both strings are equal  0 ")
	


else:
	print("Error ")
