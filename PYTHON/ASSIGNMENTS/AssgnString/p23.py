'''
. Write a program which accepts string from the user and
then reverse the string till first N characters without taking another
string.
Input: Hello World
N : 5
Output: olleH World

'''

str1=input("enter the string : ")
n=int(input("Enter the number : "))
str2=""
for i in range(len(str1)):
	if(i<n):
		str2=str2 + str1[n-i-1]
	else:
		str2=str2+str1[i]


print(str2)