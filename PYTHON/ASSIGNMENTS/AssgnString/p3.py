'''
Write a program which accepts sentences from the user
and print a number of small letters, capital letters and digits from that
sentence.
Input: abcDE 5Glm1 O
Output: Small:5 Capital: 4 Digits: 2
'''
str1=input("Enter the string : ")
S=0
C=0
D=0
for i in range (len(str1)):
	if('A'<=str1[i] and str1[i]<='Z'):
		C+=1
	elif('a'<=str1[i] and str1[i]<='z'):
		S+=1
	elif('0'<=str1[i] and str1[i]<='9'):
		D+=1

print("Small characters in string : ",S)
print("Capital characters in string : ",C)
print("Digit in string : ",D)

