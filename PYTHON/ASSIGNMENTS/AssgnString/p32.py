'''
Write a program which accepts string from the user and
searches for the first occurrence of a specific character in string and
returns the position at which character is found (Implement strchr()).
Input: India is my country.
Enter Char : m
Output: Character m is found at position 9
'''

str1= input("Enter the string : ")

a= input("Enter the char to find : ")
flag=0
for i in range (len(str1)):
	if(str1[i]==a):
		print("Character",a,"found at position :", i)
		flag+=1
		break
if(flag==0):
	print("The Character",a,"is not present in string ")
