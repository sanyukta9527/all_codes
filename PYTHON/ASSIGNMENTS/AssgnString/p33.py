'''
Write a program which accepts string from user and
search last occurrence of specific character in string and return the
position at which character is found (Implement strchr()).
Input: India is my country.
Enter char : n
Output: Character n is found at position 15
'''

str1= input("Enter the string : ")
a= input("Enter the char to find : ")
flag=0
index=0
for i in range (len(str1)):
	if(str1[i]==a):
		index=i
		flag+=1
if(flag==0):
	print("The Character",a,"not present in string ")
else:
	print("Character",a,"found at position :", index)
