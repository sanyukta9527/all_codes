'''
 Write a program which accepts sentences from the user
and print a number of white spaces from that sentence.
Input: In my company
Output: 2
'''

str1=input("Enter the string : ")
ws=0

for i in range (len(str1)):
	if(str1[i]==" "):
		ws+=1

print( "The Space in the string is : " ,ws)
