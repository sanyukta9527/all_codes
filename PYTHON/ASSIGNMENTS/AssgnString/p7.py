'''
. Write a program which accepts sentences from the user
and print a number of words of even and odd length from that
sentence.
Input: In my company
Output: Even: 2 Odd:1
'''
str1= input("Enter the string : ")
wc=0
even=0
odd=0
for i in range (len(str1)):
	if(str1[i]!=' '):
		wc+=1
	else:
		if(wc%2==0):
			even+=1
		else:
			odd+=1
		wc=0
if(wc%2==0):
	even+=1
else:
	odd+=1

print("even : ", even)
print("odd : ", odd)


