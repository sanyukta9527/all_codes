'''
Write a program which accepts sentences from the user
and prints the last word from that sentence.
Input: In my company
Output: company
'''

str1= input("Enter the string : ")
str2=""
a=0

# rfind
for i in range (len(str1)):
	if(str1[i]==" "):
		a=i

for j in range(a+1,len(str1)):
	str2=str2+str1[j]

print("The entered string is : " ,str1)
print("The last word of the entered string is : " , str2)
