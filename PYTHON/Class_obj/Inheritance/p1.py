class abc :
	def __init__(self):
		print("In parent constructor :")
		self.a=20

class xyz(abc):
	def __init__(self):
		super().__init__()
		print("in child constructor ")
		self.x=10
	def disp(self):
		print(self.x)
		print(self.a)

obj=xyz()
obj.disp()
