class parent:
	x=10
	def __init__(self):
		print('In parent')
	def disp(self,n):
		print(n)
		print('P')

class child(parent):
	x=20
	def __init__(self):
		print('In child')
		super().__init__()
	def disp(self):
		print(super().x)
		print('C')
		super().disp(super().x)

obj=child()
obj.disp()