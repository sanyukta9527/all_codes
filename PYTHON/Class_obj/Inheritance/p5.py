class parent:
	def __init__(self):
		print('In parent')
		self.x=10
		self._y=20
		self.__z=30
	def show(self):
		print(self.x)
		print(self._y)
		print(self.__z)
class child(parent):
	def __init__(self):
		print('In child')
		super().__init__()
	def disp(self):
		print(self.x)
		print(self._y)
		print(self.__z)
obj=child()
'''
obj.show()

obj.disp()'''
print(obj._parent__z)
print(dir(obj))
print(obj._y)
print(obj.__z)
