class abc:
	def __init__(self):
		pass
	def myclassmethod(fun):
		def inner(add):
			fun(add.__class__)
		return inner
	@myclassmethod
	def adde(self):
		print(self)
def func():
	pass
obj=abc()
obj.adde()