def even(fun):
	def inner(x):
		ans=fun(x)
		if(ans%2==0):
			print(ans ," is even")
		else:
			print(ans ,"is odd" )


	return inner

@even
def num(a):
	return a

a= int(input("Enter the number : "))
num(a)