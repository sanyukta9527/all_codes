'''
1   2   9   4

25  6   49  8

81  10  121 12

'''

r = int(input("Enter the number of rows :"))
c= int(input("Enter the number of col :" ))

n=1
for i in range (r):
	for j in range(c):
		if(j%2==0):
			print(n*n,end=" ")
		else:
			print(n,end=" ")
		n+=1
	print()
