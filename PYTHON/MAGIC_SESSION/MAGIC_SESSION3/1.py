'''
find the factorials of all numbers in given range 

'''


lim1 = int(input("Enter the number : "))
lim2 = int(input("Enter the last number : "))

if(lim1>=0 and lim2>=0):
	for i in range(lim1 , lim2+1):
		n=i
		fact=1
		for j in range(1 , n+1):
			fact*=j
		print("Factorial of ",i,"=", fact)
else:
	print("negetive numbers cannot be included in range ")
