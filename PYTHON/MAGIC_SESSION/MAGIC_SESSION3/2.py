'''

take number in range : multiply all odd numbers within range
					 : add all even numbers within range

'''

lim1 = int(input("Enter the starting limit : ")) 
lim2 = int(input("Enter the ending limit : "))

add=0
mul=1
for i in range (lim1 , lim2+1):
	if(i%2==0):
		add+=i
	else:
		mul*=i
print("Sum of all even numbers in range",lim1,"-",lim2,"=",add)
print("Multiplication of all odd numbers in range",lim1,"-",lim2,"=",mul)
