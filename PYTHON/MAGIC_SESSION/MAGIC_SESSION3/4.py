'''
WAP TO PRINT NUMBERS IN A RANGE WHOSE ONE OF THE PERFECT DIVISOR IS 4

'''

lim1 = int(input("Enter the starting number :"))
lim2 = int(input("Enter the ending number :"))

for i in range (lim1,lim2+1):
	if (i%4==0):
		print(i ,end=" ")
print()
