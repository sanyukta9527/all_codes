'''
+++++++++Write a Program that prints the Series of palindrome ranging in
300 to 600
Output: 313, 323, 333, 343 . . .
'''
l1=int(input("Enter the Starting : "))
l2=int(input("Enter the Ending : "))

for i in range(l1,l2+1):

	temp=i
	rem=0
	rev=0
	while(temp>0):
		rem=temp%10
		rev=(rev*10)+rem
		temp//=10
	if(i==rev):
		print(i)