'''
: Write a Program that accepts a number from the user and prints
the minimum digit from that number.
Input: 12357798
Output: The Min Digit from number 12357798 is 1
'''
num=int(input("Enter the number : "))
temp =num
min=9
while(temp>0):
	rem=temp%10
	if(min>rem):
		min=rem
	temp//=10
print("The minimum digit in the number",num , "is",min)

