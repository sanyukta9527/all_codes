'''
: Write a Program to Print following Pattern.
0 0 0 0 0
1 2 3 4
4 6 8
9 12
16
'''

for i in range (5):
	a=i*i
	for j in range(5-i):
		print(a,end=" ")
		a=a+i
	print()