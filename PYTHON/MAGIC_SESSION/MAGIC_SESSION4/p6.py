'''
: Write a Program that prints the Armstrong numbers ranging in 1
to 100.
'''

l1=int(input("Enter the number : "))
l2=int(input("Enter the number : "))
flag=0
for i in range(l1,l2+1):
	temp=i
	temp1=i
	count=0
	while(temp>0):
		count+=1
		temp//=10
	sum=0
	while(temp1>0):
		rem=temp1%10
		sum=sum+rem**count
		temp1//=10
	if(sum==i):
		print(i)
		flag+=1
if(flag==0):
	print("no armstrong number found")

