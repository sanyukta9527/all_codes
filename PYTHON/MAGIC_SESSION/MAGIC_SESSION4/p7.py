'''
: Write a Program that calculates Square Root of a number entered
by the user.
Input: 16
Output: Square Root of 16 is 4
'''
num = int(input("Enter the number : "))
flag=0
for i in range(num//2):
	if(i*i==num):
		print("The square root of ",num ,"is",i )
		flag+=1
if(flag==0):
	print("No perfect square root found")