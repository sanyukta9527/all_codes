'''
: Write a Program that prints frequency of each unique digit from a
number provided by the user.
Input: 1231234
Output:
Frequency of 1 is 2
Frequency of 2 is 2
Frequency of 3 is 2
Frequency of 4 is 1
'''
num=int(input("Enter the number "))
temp = num
temp1=num
lst=[0]

while(temp>0):
	rem=temp%10
	flag=0
	for i in range(len(lst)):
		if(rem==lst[i]):
			flag+=1
			continue
	if(flag==0):
		lst.append(rem) 
	temp//=10
lst.pop(0)
for i in range (len(lst)-1,-1,-1):
	count=0
	temp1=num
	while(temp1>0):
		rem=temp1%10
		if(lst[i]==rem):
			count+=1
		temp1=temp1//10
	print("The frequency of ",lst[i],'is',count)


