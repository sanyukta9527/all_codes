import numpy

arr1 = numpy.linspace(1,10)
#print(arr1)


#arange
arr3=numpy.array(['ABC','DEF'])
arr2=numpy.arange(ord('A'),ord('Z'))
print(arr3[0][0])
print(arr3.ndim)


#ZEROS
arr4 =numpy.zeros(10,int)
print(arr4)

#ONES
arr5=numpy.ones(10,str)
print(arr5)
