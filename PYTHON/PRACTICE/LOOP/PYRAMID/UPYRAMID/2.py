'''
          1
       2  3  4 
    5  6  7  8  9
10 11 12 13 14 15 16
'''
r = int(input("Enter the rows : "))
a=1
for i in range(r):
	for j in range(r-i-1):
		print("  ",end=" ")
	for k in range(2*i+1):
		if(a<10):
			print(a,end="  ")
		else:
			print(a,end=" ")

		a+=1
	print( )