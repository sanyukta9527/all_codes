'''
      1
    2 1 2
  3 2 1 2 3
4 3 2 1 2 3 4
'''

r = int(input("Enter the number : "))
a=1
for i in range(r):
	for j in range(r-i-1):
		print(" ",end=" ")
	
	for k in range(2*i+1):
		print(a,end=' ')
		if(k<i):
			a-=1
		else:
			a+=1

	print()
