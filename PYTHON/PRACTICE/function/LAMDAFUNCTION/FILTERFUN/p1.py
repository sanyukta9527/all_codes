# Syntax : filter(function,sequence)
'''def oddeven(x):
	if(x%2==0):
		return True
	else:
		return False
lst=[1,2,3,4,5,6,7,8,9,10]
for i in lst:
	ret=oddeven(i)
	if(ret==True):
		print(i)
'''
lst=[1,-1,4,-3,8,-9,7,-5,-6,-1]
for i in filter(lambda i:i<0,lst):
	print(i)