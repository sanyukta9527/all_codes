'''Shallow copy and deep copy

import array

arr=array.array("i",[4,6,3,8,2,9])
arr1=arr

print(arr)
print(arr1)
'''
lst1=[1,2,3]
lst2=list()
lst3=list()
lst2=lst1
lst2[0]=10
for i in range(len(lst1)):
	lst3.append(lst1[i])
lst3[0]=40
print(lst1)
print(lst2)
print(lst3)